
create user bogoe identified by bogotae;

CREATE TABLE tipos_emprendimiento
( id number(10) NOT NULL,
  descripcion varchar2(100) NOT NULL,
  CONSTRAINT id_tipo_empren_pk PRIMARY KEY (id)
); 

CREATE TABLE emprendedores
( id number(10) NOT NULL,
  nombres varchar2(100) NOT NULL,
  apellidos varchar2(100) NOT NULL,
  nombre_emprendimiento varchar2(100) NOT NULL,
  desc_emprendimiento varchar2(500) NOT NULL,
  correo_electronico varchar2(60) NOT NULL,
  fecha_nacimiento timestamp NOT NULL,
  fecha_creacion timestamp NOT NULL,
  id_tipo_emprendimiento number(10) NOT NULL,
  CONSTRAINT id_pk PRIMARY KEY (id),
  CONSTRAINT fk_id_tipo_emprendim FOREIGN KEY (id_tipo_emprendimiento) REFERENCES tipos_emprendimiento(id)
);


CREATE SEQUENCE SEC_EMPRENDEDORES MINVALUE 1 START WITH 1 INCREMENT BY 1 NOCACHE;
CREATE SEQUENCE SEC_TIPOS_EMPRENDIMIENTO MINVALUE 1 START WITH 1 INCREMENT BY 1 NOCACHE;
