# BogotaE

Prueba técnica Plataforma Bogotá-e

Este proyecto presenta la implementación de un sistema Bogotá-E, con el uso de Spring Boot, Java, JPA, y base de datos Oracle.
A continuación, se comentan las funcionalidades del sistema.
Se separó la solución a nivel de base de datos con el uso de dos tablas, una que almacena emprendedores y otra que almacena tipos de emprendimiento. Así mismo se creados dos servicios RestFull para la administración de estas dos entidades.

1.	Se puede realizar el registro de nuevos emprendedores por medio de la ruta:

http://localhost:8080/api/emprendedores

Con sus respectivos métodos:
- GET: Consultar los emprendedores y los ordena por fecha de registro.
- POST: almacenar nuevos emprendedores con sus respectivos emprendimientos.
- PUT: Permite actualizar los registros de emprendedores. 
- DELETE: Eliminar a emprendedores con sus respectivos emprendimientos. (Recibe solo el id del emprendedor)
2.	Se puede realizar la consulta de los tipos de emprendimiento por medio de la ruta: 

	http://localhost:8080/api/tiposemp

Con sus respectivos métodos: 
- GET: Consultar la lista de tipos de emprendimiento.
- POST: almacenar nuevos tipos de emprendimiento.
- PUT: Permite actualizar los registros de tipos de emprendimiento. (Recibe solo el id del Tipos emprendimiento)
- DELETE: Eliminar tipos de emprendimiento.

3. Con la ruta http://localhost:8080/api/emprendedores y utilizando el metodo PUT se puede actualizar la informacion del emprendedor

4.	Por medio del método GET de la ruta http://localhost:8080/api/emprendedores se realiza el listado de todos los emprendedores registrados ordenados por fecha de registro.

5.	Para buscar un emprendimiento por nombre de emprendimiento se puede realizar por medio de la siguiente ruta:
http://localhost:8080/api/emprendedores/emprendimiento/NombreEmprendimiento


A continuación, algunos JSON que sirven como ejemplo para la creación de registros.

Tipos_emprendimientos

    {  
        "descripcion": "Social"
    }
     { 
        "descripcion": "Económico"
    }
     { 
        "descripcion": "Ambiental"
    }
     { 
        "descripcion": "Publico"
    }
    { 
        "descripcion": "Tecnológico"
    }
    {  
        "descripcion": "Digital"
    }
    {       
        "descripcion": "Productivo"
    }

Emprendedor

    {        
        "nombres": "diego",
        "apellidos": "izquierdo",
        "nombreEmprendimiento": "L’olivera",
        "descEmprendimiento": "En el pueblo de Vallbona de les Monges, en Lérida, funciona la cooperativa L’olivera desde 1974",
        "email": "lolivera@gmail.com",
        "fechaNacimiento": "1990-08-04T01:15:04.032+00:00",
        "idTipoEmprend": 1
    }
    {        
        "nombres": "Jose Manuel",
        "apellidos": "Moller",
        "nombreEmprendimiento": "Algramo",
        "descEmprendimiento": "impulsa la distribución de dispensadores automáticos a granel de productos alimenticios y de otros tipos",
        "email": "Algramo@gmail.com",
        "fechaNacimiento": "1980-07-04T01:15:04.032+00:00",
        "idTipoEmprend": 1
    }
    {        
        "nombres": "Luisa",
        "apellidos": "Hernandez",
        "nombreEmprendimiento": "Encore",
        "descEmprendimiento": "busca conectar a estas personas con becas, cursos y empleos; una bella forma de ofrecer una nueva motivación para sus vidas.",
        "email": "Encore@gmail.com",
        "fechaNacimiento": "1997-07-04T01:15:04.032+00:00",
        "idTipoEmprend": 2
    }
        {        
        "nombres": "Andres",
        "apellidos": "Perez",
        "nombreEmprendimiento": "Proplanet",
        "descEmprendimiento": " se destaca por la elaboración de múltiples productos que van desde elementos de construcción, mobiliario urbano y escolar, macetas, entre otros",
        "email": "propanet@gmail.com",
        "fechaNacimiento": "2015-07-04T01:15:04.032+00:00",
        "idTipoEmprend": 3
    }
    {        
        "nombres": "Juan",
        "apellidos": "Raul",
        "nombreEmprendimiento": "Cylus ",
        "descEmprendimiento": "Su producto estrella son bolsos diseñados con materiales urbanos, como el caucho de los neumáticos usados, al cual le aplican diversos procesos de limpieza para dejarlos libres de químicos.",
        "email": "prueba",
        "fechaNacimiento": "2015-07-04T01:15:04.032+00:00",
        "idTipoEmprend": 4
    }
    {
        "nombres": "Jeff",
        "apellidos": "Bezos",
        "nombreEmprendimiento": "Amazon",
        "descEmprendimiento": "Amazon aspira a liderar la venta por internet en todas las categorías de productos...",
        "email": "amazon@gmail.com",
        "fechaNacimiento": "2015-07-04T01:15:04.032+00:00",
        "idTipoEmprend": 6
    }
